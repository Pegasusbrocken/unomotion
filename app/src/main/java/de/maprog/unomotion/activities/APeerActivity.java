package de.maprog.unomotion.activities;

import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import de.maprog.unomotion.fragments.GameFragment;
import de.maprog.unomotion.peer.PeerReceiver;

/**
 * Created by Konstantin on 26.11.2016.
 * Abstrakte Klasse für die Connect- und Hostactivities.
 * Beeinhaltet wichtige Funktionen für Wifi Direct
 *
 */
public abstract class APeerActivity extends AppCompatActivity
{
    IntentFilter intentFilter = new IntentFilter();
    WifiP2pManager mManager;
    WifiP2pManager.Channel mChannel;
    PeerReceiver peerReceiver;
    String host;
    int groupOwner = 15;                 //if 0 -> Client, else Host
    public WifiP2pManager.PeerListListener peerListListener;
    public ListView listView;
    public ArrayAdapter<String> adapter;

    ArrayList<Socket> clientSockets = new ArrayList<>();
    Socket hostSocket;
    GameFragment gameFragment;
    boolean isSearching =true;


    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public Socket getHostSocket()
    {
        return hostSocket;
    }

    public void setHostSocket(Socket hostSocket)
    {
        this.hostSocket = hostSocket;
    }

    public IntentFilter getIntentFilter()
    {
        return intentFilter;
    }

    public void setIntentFilter(IntentFilter intentFilter)
    {
        this.intentFilter = intentFilter;
    }

    public WifiP2pManager getmManager()
    {
        return mManager;
    }

    public void setmManager(WifiP2pManager mManager)
    {
        this.mManager = mManager;
    }

    public WifiP2pManager.Channel getmChannel()
    {
        return mChannel;
    }

    public void setmChannel(WifiP2pManager.Channel mChannel)
    {
        this.mChannel = mChannel;
    }

    public int getGroupOwner()
    {
        return groupOwner;
    }

    public void setGroupOwner(int groupOwner)
    {
        this.groupOwner = groupOwner;
    }

    public abstract void startListening();

    public void addFilters()
    {
        //  Indicates a change in the Wi-Fi P2P status.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);

        // Indicates a change in the list of available peers.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);

        // Indicates the state of Wi-Fi P2P connectivity has changed.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);

        // Indicates this device's details have changed.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
    }

    public void search()
    {
        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener()
        {
            @Override
            public void onSuccess()
            {
                Toast.makeText(getApplication(), "Suche...", Toast.LENGTH_SHORT).show();
                //Peers_Changed_Action
            }

            @Override
            public void onFailure(int reason)
            {
                Toast.makeText(getApplication(), "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void addConnectedDevices(String deviceName)
    {
        adapter.add(deviceName);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
    }
}
