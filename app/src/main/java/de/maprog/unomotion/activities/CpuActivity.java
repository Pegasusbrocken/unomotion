package de.maprog.unomotion.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.net.Socket;

import de.maprog.unomotion.R;
import de.maprog.unomotion.fragments.CpuFragment;
import de.maprog.unomotion.fragments.GameFragment;

public class CpuActivity extends AppCompatActivity
{
    CpuFragment cpuFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cpu);
    }


    public void aiPressed(View view)
    {
        view.setVisibility(View.GONE);

        cpuFragment = CpuFragment.newInstance(true, 1 + 1, 0);      //Fragment erzeugen (host, spielerzahl todo, spielernummer)
        FragmentManager fm = getFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.cpuFrameLayout, cpuFragment);             //Fragment austauschen
        ft.commit();                                            //un
    }
}
