package de.maprog.unomotion.activities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.UUID;

import de.maprog.unomotion.R;

/**
 * Created by Konstantin on 25.10.2016.
 * Aktiviert die Bluetoothfunktionalität des Gerätes, da diese in den darauffolgenden Activities benötigt wird.
 * Falls kein Bluetooth vorhanden ist, oder der Nutzer die Aktivierung ablehnt, wird in die MainActivity zurückgesprungen.
 */
public class PlayActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
    }

    public void hostPressed(View view)
    {
        Intent intent = new Intent(this,HostActivity.class);
        startActivity(intent);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void connectPressed(View view)
    {
        Intent intent = new Intent(this,ConnectActivity.class);
        startActivity(intent);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void aiPressed(View view)
    {
        Intent intent = new Intent(this,CpuActivity.class);
        startActivity(intent);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
