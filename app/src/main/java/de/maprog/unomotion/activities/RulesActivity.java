package de.maprog.unomotion.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import de.maprog.unomotion.R;

/**
 * Created by Konstantin on 25.10.2016.
 * Zeigt die Spielregeln an.
 */
public class RulesActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules);
    }
}
