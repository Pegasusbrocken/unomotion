package de.maprog.unomotion.activities;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import de.maprog.unomotion.R;

/**
 * Created by Konstantin on 25.10.2016.
 * Start-Activity, mit zwei Buttons zum Weiterleiten
 * <TODO: Sensormanager in passende Klasse/Activity umziehen & @Peter Funktionsweise vom Sensor kommentieren></TODO:>
 */
public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void playPressed(View view)
    {
        Intent intent = new Intent(this, PlayActivity.class);
        startActivity(intent);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void rulesPressed(View view)
    {
        Intent intent = new Intent(this, RulesActivity.class);
        startActivity(intent);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

    }

}
