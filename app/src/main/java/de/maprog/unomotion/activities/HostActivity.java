package de.maprog.unomotion.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.net.Uri;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;

import de.maprog.unomotion.R;
import de.maprog.unomotion.fragments.GameFragment;

/**
 * Created by Konstantin on 12.11.2016.
 * Macht Gerät für andere WifiDirect-Geräte sichtbar und wartet dann auf Verbindungen.
 */
public class HostActivity extends APeerActivity implements GameFragment.OnFragmentInteractionListener
{
    private ArrayList<Socket> connections = new ArrayList<>();
    TextView hostTextViewDeviceName, hostTextViewHeader;
    ServerSocketTask serverSocketTask;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_host);

        setGroupOwner(15);
        setmManager((WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE));
        setmChannel(getmManager().initialize(this, getMainLooper(), null));
        addFilters();

        hostTextViewDeviceName = (TextView) findViewById(R.id.hostTextViewDeviceName);
        hostTextViewHeader = (TextView) findViewById(R.id.hostTextViewHeader);

        listView = (ListView) findViewById(R.id.connectListViewDevices);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);    //Für die (später) verbundenen Devices
        listView.setAdapter(adapter);

        peerListListener = new WifiP2pManager.PeerListListener()
        {
            @Override
            public void onPeersAvailable(WifiP2pDeviceList peers)
            {
            }
        };

        search();

        if(savedInstanceState == null)
        {
            serverSocketTask = new ServerSocketTask();
            serverSocketTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

    }

    public void startGame(View view)
    {
        signalStartToSockets();             //Gib verbundenen Geräten das Startsignal
        listView.setVisibility(View.GONE);          //Alle anderen Layoutelemente unsichtbar
        view.setVisibility(View.GONE);
        hostTextViewDeviceName.setVisibility(View.GONE);
        hostTextViewHeader.setVisibility(View.GONE);

        gameFragment = GameFragment.newInstance(true, clientSockets.size() + 1,0);      //Fragment erzeugen (host, spielerzahl, spielernummer)
        gameFragment.setClientSockets(clientSockets.toArray(new Socket[clientSockets.size()]));     //Sockets übergeben

        FragmentManager fm = getFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.hostFrameLayout, gameFragment);             //Fragment austauschen
        ft.commit();                                            //und ausführen
    }

    private void signalStartToSockets()
    {
        Iterator<Socket> it = clientSockets.iterator();
        int playerNumber = 1;

        while(it.hasNext())
        {
            Socket curr = it.next();
            new WriteTask(curr,playerNumber).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"Start");
            playerNumber++;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri)
    {

    }

    @Override
    public void startListening()
    {
            if(serverSocketTask == null || serverSocketTask.isCancelled())
            {
                serverSocketTask = new ServerSocketTask();
                serverSocketTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
    }

    private class ServerSocketTask extends AsyncTask<Void, Socket, String>
    {
        @Override
        protected String doInBackground(Void... params)
        {
            try
            {
                ServerSocket serverSocket = new ServerSocket(8888);

                while(true)
                {
                    Socket client = serverSocket.accept();
                    publishProgress(client);

                    if(clientSockets.size() >= 3)
                        break;
                }
                serverSocket.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return "ServerSocket closed";
        }

        @Override
        protected void onProgressUpdate(Socket... params)
        {
            clientSockets.add(params[0]);
            //new ReadTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,params);
        }

        @Override
        protected void onPostExecute(String s)
        {
        }
    }

    private class WriteTask extends AsyncTask<String, Void, Void>
    {
        Socket socket;
        int playerNumber;

        public WriteTask(Socket socket, int playerNumber)
        {
            this.socket = socket;
            this.playerNumber = playerNumber;
        }

        @Override
        protected Void doInBackground(String... params)
        {
            try
            {
                OutputStream os = socket.getOutputStream();
                OutputStreamWriter osWriter = new OutputStreamWriter(os);

                osWriter.write(params[0] + ":" +playerNumber);
                osWriter.flush();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
        }
    }

}
