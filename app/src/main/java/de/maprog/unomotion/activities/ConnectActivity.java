package de.maprog.unomotion.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import de.maprog.unomotion.R;
import de.maprog.unomotion.fragments.GameFragment;
import de.maprog.unomotion.peer.PeerReceiver;

/**
 * Created by Konstantin on 12.11.2016.
 * Sucht nach mit hilfe von Wifi Direct nach möglichen Geräten und zeigt diese in einer Liste an, so dass sich
 * verbunden werden kann.
 */
public class ConnectActivity extends APeerActivity
{
    TextView connectTextViewChooseDevice;
    Button connectButtonSearchDevice;
    ListView listView;
    ArrayAdapter<WifiP2pDevice> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        setGroupOwner(0);                                                               //Das Gerät mit dem höheren Wert ist Host, daher für den Client 0
        setmManager((WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE));       //Wifi Direct initialisieren
        setmChannel(getmManager().initialize(this, getMainLooper(), null));
        addFilters();                                                                   //Filter für Wifi Direct, ausgelagert, damit übersichtlicher

        listView = (ListView) findViewById(R.id.connectListViewDevices);        //Layoutelement holen für die (später) gefundenen Devices
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);    //android.R.layout.simple_list_item_1 = Art der Listendarstellung
        listView.setAdapter(adapter);

        peerListListener = new WifiP2pManager.PeerListListener()
        {
            @Override
            public void onPeersAvailable(WifiP2pDeviceList peers)
            {           //Listener, der auf neue Peers horcht, der Listener wird ausgeführt nachdem eine Suche nach Wifi Direct Geräten vollzogen wurde
                adapter.clear();        //Alle bereits angezeigten Elemente aus der Liste entfernen, damit der User sie nicht mehr sehen kann
                adapter.addAll(peers.getDeviceList());  //Alle gefundenen Elemente hinzufügen
                adapter.notifyDataSetChanged(); //Refresh
            }
        };

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()          //Listener für jedes ListenObjekt
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id)
            {
                WifiP2pDevice device = (WifiP2pDevice) listView.getItemAtPosition(position);
                new ConnectTask().execute(device);              //Starte AsyncTask zum Verbinden mit dem gewählten Device
            }

        });

        connectTextViewChooseDevice = (TextView) findViewById(R.id.connectTextViewChoseDevice); //Layoutelement zuweisen
        connectButtonSearchDevice = (Button) findViewById(R.id.connectButtonSearchDevice);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        peerReceiver = new PeerReceiver(mManager, mChannel, peerListListener, this);   //Receiver erstellen & registieren
        registerReceiver(peerReceiver, intentFilter);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        unregisterReceiver(peerReceiver);           //Receiver wieder entfernen
    }

    @Override
    public void startListening()
    {
        Log.i("ConnectActivity", "startListening() is not used in ConnectActivity");    //Wird vererbt, aber nicht benutzt, daher zumindest ein kleiner InfoLog, sollte es trotzdem aufgerufen werden
    }

    public void search(View view)
    {
        search();
    }   //Damit im Xml benutzbar (View view)

    public void connectToSocket(String host)
    {
        ConnectToSocketTask connectToSocketTask = new ConnectToSocketTask();            //Startet eigenen AsyncTask zum verbinden mit dem Host
        connectToSocketTask.host = host;                                            //Host setzten
        connectToSocketTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);      //Wie sich nach mehreren Stunden Debuggings herausgestellt hat, erlaubt Android erstmal nur einen AsyncTask gleichzeitig.
                                                                                    //Durch die Verwendung von executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR) lassen sich mehrere Tasks erzwingen
    }

    /**
     *  AsyncTask zum verbinden mit anderem Wifi Direct Gerät
     *  Kümmert sich NICHT um die Socketverbindung
     */
    private class ConnectTask extends AsyncTask<WifiP2pDevice, Void, Void>
    {
        @Override
        protected Void doInBackground(WifiP2pDevice... params)
        {
            WifiP2pDevice device = params[0];   //Das gewünschte Gerät
            WifiP2pConfig config = new WifiP2pConfig(); //Verbindungseinstellungen

            config.deviceAddress = device.deviceAddress;
            config.wps.setup = WpsInfo.PBC;
            config.groupOwnerIntent = getGroupOwner();
            mManager.connect(mChannel, config, new WifiP2pManager.ActionListener()
            {
                @Override
                public void onSuccess()
                {
                    // PeerReceiver bekommt WIFI_P2P_CONNECTION_CHANGED_ACTION
                }

                @Override
                public void onFailure(int reason)
                {
                    Toast.makeText(getApplication(), "Connection failed, please restart the application.", Toast.LENGTH_SHORT).show();
                }
            });

            return null;
        }
    }

    /**
     *  AsyncTask zum verbinden mit Socket
     */
    private class ConnectToSocketTask extends AsyncTask<Void,Socket,Void>
    {
        String host;
        int port = 8888;        //War bei keinem getesteten Gerät belegt

        @Override
        protected Void doInBackground(Void... params)
        {
            Socket socket = new Socket();
            try
            {
                socket.bind(null);
                socket.connect((new InetSocketAddress(this.host,this.port)),35000);         //connect(adresse, timeout)
                setHostSocket(socket);          //"speicher socket als Verbindung zum Host"
                publishProgress(socket);        //onProgressUpdate -> Ui Thread
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Socket... params)
        {
            new ReadTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,params);       //Neuer Task um auf Socket zu horchen, Muss auf Ui Thread geschehen
        }
    }

    /**
     *  AsyncTask zum Auslesen des Sockets
     *  Wartet eigentlich nur darauf, dass der Host das OK zum Spielstart schickt
     */
    private class ReadTask extends AsyncTask<Socket, Void, String>
    {
        @Override
        protected String doInBackground(Socket... params)
        {
            byte[] buffer = new byte[2048];
            int curr;

            if (params.length == 0)
                return null;

            try
            {
                StringBuffer buff = new StringBuffer();
                InputStream is = params[0].getInputStream();

                while ((curr = is.read(buffer)) != -1)
                {
                    String output = new String(buffer, 0, curr);
                    buff.append(output);

                    if(buff.toString().contains("Start"))
                        return(buff.toString().substring(buff.toString().indexOf(":")+1, buff.toString().length()));    //Nummer des Spielers
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String s)
        {
            if(s == null)
                return;

            listView.setVisibility(View.GONE);                  //Vorhandene Ui Elemente verschwinden lassen
            connectButtonSearchDevice.setVisibility(View.GONE);
            connectTextViewChooseDevice.setVisibility(View.GONE);

            gameFragment = GameFragment.newInstance(false, 0, Integer.parseInt(s));     //Das Spielfeld erzeugen keine Host, Spieleranzahl =0, da egal für Client, und Nummer des Spielers
            gameFragment.setServerSocket(hostSocket);           //Dem Spielfeldfragment den Hostsocket mitteilen

            FragmentManager fm = getFragmentManager();

            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.connectFrameLayout, gameFragment);  //Das Spielfragment einfügen
            ft.commit();                                        //ausführen
        }
    }
}
