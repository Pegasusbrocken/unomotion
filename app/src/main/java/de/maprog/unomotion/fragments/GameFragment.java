package de.maprog.unomotion.fragments;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.maprog.unomotion.R;
import de.maprog.unomotion.utility.Card;
import de.maprog.unomotion.utility.GameControl;
import de.maprog.unomotion.utility.UnoButton;
import de.maprog.unomotion.utility.Json;

/**
 * Diese Klasse ist das Spielfeld an sich
 */
public class GameFragment extends Fragment
{
    private static final String IS_HOST = "host";
    private static final String PLAYER_NUMBER = "playerNumber";
    private static GameControl gameControl;
    private boolean host;
    private Socket[] clientSockets;
    private int numberOfClients;
    private int playerNumber;
    private Socket serverSocket;
    SensorManager mgr;
    int position_sign = 1;

    private HorizontalScrollView horizontalScrollView;
    private TextView status;

    private OnFragmentInteractionListener mListener;

    public GameFragment()
    {
    }

    public static GameFragment newInstance(boolean host, int players, int playerNumber)
    {
        GameFragment fragment = new GameFragment();
        Bundle args = new Bundle();
        args.putInt(PLAYER_NUMBER, playerNumber);
        args.putBoolean(IS_HOST, host);
        if (host)
            gameControl = new GameControl(players);

        fragment.setArguments(args);
        return fragment;
    }


    public void setServerSocket(Socket serverSocket)
    {
        this.serverSocket = serverSocket;
        new ReadTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverSocket);
    }

    public void setClientSockets(Socket[] clientSockets)
    {
        if (clientSockets == null || clientSockets.length == 0)
        {
            Log.e("GameFragment", "Sockettransfer failed: ");
            return;
        }

        this.clientSockets = clientSockets;
        numberOfClients = clientSockets.length;

        for (int i = 0; i < numberOfClients; i++)
        {
            new ReadTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, this.clientSockets[i]);
        }

    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            host = getArguments().getBoolean(IS_HOST);
            playerNumber = getArguments().getInt(PLAYER_NUMBER);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_game, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        Button fragmentButtonDraw = (Button) getView().findViewById(R.id.fragmentButtonDraw);
        fragmentButtonDraw.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addCard();
            }
        });
        horizontalScrollView = (HorizontalScrollView) getView().findViewById(R.id.scrollHand);
        status = (TextView) getView().findViewById(R.id.status);

        if (host)
            initField();

        mgr = (SensorManager) getView().getContext().getSystemService(Context.SENSOR_SERVICE);
        mgr.registerListener(listener, mgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);

    }

    /**
     * Initialisiert das Feld mit Handkarten und schickt die Infos an alle Clients
     */
    private void initField()
    {
        setStatus(gameControl.getTurnOfPlayer());

        Button stack = (Button) getView().findViewById(R.id.fragmentButtonStack);
        int identifier = getResources().getIdentifier("stapel_" + gameControl.getStackTopCard(), "drawable", getView().getContext().getPackageName());
        stack.setBackgroundDrawable(getResources().getDrawable(identifier));        //Grafik für gelegte Karte laden

        setCards();
        sendGameState();

    }

    /**
     * Statustext aktualisieren
     * @param playersTurn
     */
    private void setStatus(int playersTurn)
    {
        String currStatus = getString(R.string.currState);
        StringBuffer buffer = new StringBuffer();
        buffer.append(currStatus.substring(0, currStatus.indexOf(":") + 1));
        buffer.append(" " + playerNumber + " \n");
        buffer.append(currStatus.substring(currStatus.indexOf(":") + 1, currStatus.length()));
        status.setText(buffer.toString().replace("X", String.valueOf(playersTurn)));
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mgr.unregisterListener(listener);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener
    {
        void onFragmentInteraction(Uri uri);
    }

    /**
     * Schickt String zu entsprechendem Client
     * @param number
     * @param s
     */
    private void sendToClient(final int number, final String s)
    {
        Thread thread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    OutputStream os = clientSockets[number].getOutputStream();
                    OutputStreamWriter osWriter = new OutputStreamWriter(os);
                    osWriter.write(s);
                    osWriter.flush();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    private SensorEventListener listener = new SensorEventListener()
    {
        @Override
        public void onSensorChanged(SensorEvent event)                                              // Wird aufgerufen sobald sich der Sensorwert ändert.
        {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)                                // wenn Accelerometer
            {
                float x, y;
                //int position_sign = 1;
                x = event.values[0];                                                                //schreibe die Sensorwerte in die float-variablen x,y,z
                y = event.values[1];


                if (Math.abs(y) > 1)
                {                                                               // neigungsgrenzwert 1 , dass sich das nicht immer bewegt
                    horizontalScrollView.setSmoothScrollingEnabled(true);
                    if (Math.abs(x) > 1)
                        position_sign = (int) (x / Math.abs(x) * -1);                                // Positionsabhängige vorzeichenänderung um eine gleichbleibende Scrollrichtung zu gewärleisten
                    horizontalScrollView.smoothScrollBy((int) Math.pow(y, 3) * position_sign, 0); // Funktion zum Scrollen in verschiedene richtungen/in verschiedenen geschwindigkeiten, unabhängig von der ausrichtung des SP

                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy)
        {

        }
    };

    /**
     * Schickt String zum Host/Server
     * @param s
     */
    private void sendToServer(final String s)
    {
        Thread thread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    OutputStream os = serverSocket.getOutputStream();
                    OutputStreamWriter osWriter = new OutputStreamWriter(os);
                    osWriter.write(s);
                    osWriter.flush();
                }

                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    /**
     * Liest vom Socket
     */
    private class ReadTask extends AsyncTask<Socket, String, Void>
    {
        @Override
        protected Void doInBackground(Socket... params)
        {
            byte[] buffer = new byte[2048];
            int curr;

            if (params.length == 0)
                return null;

            try
            {
                InputStream is = params[0].getInputStream();

                while ((curr = is.read(buffer)) != -1)
                {
                    String output = new String(buffer, 0, curr);
                    publishProgress(output);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values)
        {
            if (host)
            {
                handleClientMessage(values[0]);
            }
            else
            {
                handleServerMessage(values[0]);
            }

        }
    }

    /**
     * Karte ziehen
     */
    private void addCard()
    {
        if (host)
        {
            clearCards();
            gameControl.handleInput(0, 1, -1, -1);
            setCards();
            sendGameState();            //Host hat gezogen und schickt die Info an alle teilnehmer
            setStatus(gameControl.getTurnOfPlayer());
        }

        else
        {
            // Hallo lieber Server, bitte ziehe eine Karte für mich. // Spielernummer // Turntype = 1, wenn karte ziehen //CardID Nothing, da wir keine karte legen   //ColorWish Nothing, da kein Color Wish
            String s = Json.JSONToServer(playerNumber, 1, -1, -1).toString();
            sendToServer(s);
        }
    }

    /**
     * Löscht alle Karten aus dem Layout
     */
    private void clearCards()
    {
        LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.hand);
        linearLayout.removeAllViews();
    }

    /**
     * Setzt Handkarten neu
     */
    private void setCards()
    {
        if (host)
        {
            List<Card> cards;
            cards = gameControl.getCardsOfPlayer(playerNumber);
            setCards(cards);
        }
    }

    private void setCards(List<Card> cards)
    {
        LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.hand);
        UnoButton button;
        Card card;
        int identifier;
        Iterator<Card> it = cards.iterator();
        while (it.hasNext())
        {
            card = it.next();
            button = new UnoButton(getView().getContext(), card.id);
            button.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    playCard((UnoButton) v);
                }
            });
            identifier = getResources().getIdentifier("hand_" + card.id, "drawable", getView().getContext().getPackageName());
            button.setBackgroundDrawable(getResources().getDrawable(identifier));
            linearLayout.addView(button);
        }

    }

    private void playCard(UnoButton unoButton)
    {
        if (host)
        {
            if (gameControl.handleInput(0, 0, unoButton.getCardID(), -1))
            {
                updateStack(unoButton.getCardID());
                removeCardFromHand(unoButton);      //Send to clients
                sendGameState();
                setStatus(gameControl.getTurnOfPlayer());
                checkGameOver();
            }
            else        //invalid input
                return;
        }
        else
        {
            try
            {
                String s = Json.JSONToServer(
                        playerNumber, // Spielernummer
                        0,            // Turntype = 0, wenn karte legen
                        unoButton.getCardID(),            //CardID Nothing, da wir keine karte legen
                        -1             //ColorWish Nothing, da kein Color Wish                      // TODO einfügen, wenn wildcard.hasColorpicker
                ).toString();

                sendToServer(s);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private void removeCardFromHand(UnoButton unoButton)
    {
        LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.hand);
        linearLayout.removeView(unoButton);
    }

    private void updateStack(int id)
    {
        Button stack = (Button) getView().findViewById(R.id.fragmentButtonStack);
        int identifier = getResources().getIdentifier("stapel_" + id, "drawable", getView().getContext().getPackageName());
        stack.setBackgroundDrawable(getResources().getDrawable(identifier));
    }

    private void handleServerMessage(String serverResponse)        //in Client
    {
        try
        {
            JSONObject jsonResponse = Json.StringToJSON(serverResponse);
            jsonResponse.getInt("client");

            if (jsonResponse.getInt("responseType") == 1)
            {
                setStatus(jsonResponse.getInt("turn"));                                                     // Zeige an <return;> ist am Zug
                updateStack(jsonResponse.getInt("cardOnStack"));                                              // Oberste Karte aufm Ablagestapel(stack)
                jsonResponse.getInt("ColorWish");                                                // -1
                JSONArray cards = jsonResponse.getJSONArray("cards");
                List<Card> cardList = new ArrayList<>();
                int cardId;
                for (int i = 0; i < cards.length(); i++)
                {
                    cardId = cards.getInt(i);                                                    //Iteriert über das Array aus Karten und schreibt jede ID in KartenID
                    cardList.add(Card.getCardByID(cardId));
                }
                clearCards();
                setCards(cardList);
            }
            if (jsonResponse.getInt("responseType") == 3)
            {
                finishGame(getString(R.string.won));
            }
            if (jsonResponse.getInt("responseType") == 4)
            {
                finishGame(getString(R.string.lost));
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void finishGame(String message)
    {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
        getActivity().finish();
    }


    private void handleClientMessage(String clientMessage)
    {
        JSONObject clientJson = Json.StringToJSON(clientMessage);

        if (gameControl.handleResponse(clientJson))
        {
            updateStack(gameControl.getStackTopCard());
            setStatus(gameControl.getTurnOfPlayer());
            sendGameState();
            checkGameOver();

        }
    }

    private void checkGameOver()
    {
        switch (gameControl.isGameOver())
        {
            case -1:
                break;             //Wenn Spiel nicht vorbei
            case 0:
                finishGame(getString(R.string.won));
                break; //Wenn Host gewonnen

            default:
                finishGame(getString(R.string.lost)); //Wenn Host verloren
        }
    }

    private void sendGameState()
    {
        int winner = gameControl.isGameOver();
        int response = 1;
        if (winner != -1)
        {
            for (int i = 0; i < numberOfClients; i++)
            {
                response = 4;       //Jemand anderes hat gewonnen
                if (i == winner)
                    response = 3;       //Der Spieler hat gewonnen

                String message = Json.JSONToClient(i + 1, response, gameControl.getTurnOfPlayer(), gameControl.getCardsOfPlayernAsIDs(i + 1), gameControl.getStackTopCard(), gameControl.getColorWish()).toString();
                sendToClient(i, message);
            }
        }
        else
        {
            for (int i = 0; i < numberOfClients; i++)
            {
                String message = Json.JSONToClient(i + 1, response, gameControl.getTurnOfPlayer(), gameControl.getCardsOfPlayernAsIDs(i + 1), gameControl.getStackTopCard(), gameControl.getColorWish()).toString();
                sendToClient(i, message);
            }
        }
    }
}
