package de.maprog.unomotion.fragments;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

import de.maprog.unomotion.R;
import de.maprog.unomotion.utility.Card;
import de.maprog.unomotion.utility.CpuPlayer;
import de.maprog.unomotion.utility.GameControl;
import de.maprog.unomotion.utility.Json;
import de.maprog.unomotion.utility.UnoButton;


public class CpuFragment extends android.app.Fragment
{
    private static final String IS_HOST = "host";
    private static final String PLAYER_NUMBER = "playerNumber";
    private static GameControl gameControl;
    private boolean host;
    private int playerNumber, cpuNumber;
    SensorManager mgr;
    int position_sign = 1;

    private HorizontalScrollView horizontalScrollView;
    private TextView status;

    private GameFragment.OnFragmentInteractionListener mListener;

    public CpuFragment()
    {
    }

    public static CpuFragment newInstance(boolean host, int players, int playerNumber)
    {
        CpuFragment fragment = new CpuFragment();
        Bundle args = new Bundle();
        args.putInt(PLAYER_NUMBER, playerNumber);
        args.putBoolean(IS_HOST, host);
        if (host)
            gameControl = new GameControl(players);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            host = getArguments().getBoolean(IS_HOST);
            playerNumber = getArguments().getInt(PLAYER_NUMBER);
            cpuNumber = 1;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_game, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        Button fragmentButtonDraw = (Button) getView().findViewById(R.id.fragmentButtonDraw);
        fragmentButtonDraw.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                drawCard();
            }
        });
        horizontalScrollView = (HorizontalScrollView) getView().findViewById(R.id.scrollHand);
        status = (TextView) getView().findViewById(R.id.status);

        if (host)
            initField();

        mgr = (SensorManager) getView().getContext().getSystemService(Context.SENSOR_SERVICE);
        mgr.registerListener(listener, mgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);

    }

    /**
     * Initialisiert das Feld mit Handkarten und schickt die Infos an alle Clients
     */
    private void initField()
    {
        setStatus(gameControl.getTurnOfPlayer());

        Button stack = (Button) getView().findViewById(R.id.fragmentButtonStack);
        int identifier = getResources().getIdentifier("stapel_" + gameControl.getStackTopCard(), "drawable", getView().getContext().getPackageName());
        stack.setBackgroundDrawable(getResources().getDrawable(identifier));        //Grafik für gelegte Karte laden

        setCards();
    }

    /**
     * Statustext aktualisieren
     *
     * @param playersTurn
     */
    private void setStatus(int playersTurn)
    {
        String currStatus = getString(R.string.currState);
        StringBuffer buffer = new StringBuffer();
        buffer.append(currStatus.substring(0, currStatus.indexOf(":") + 1));
        buffer.append(" " + playerNumber + " \n");
        buffer.append(currStatus.substring(currStatus.indexOf(":") + 1, currStatus.length()));
        status.setText(buffer.toString().replace("X", String.valueOf(playersTurn)));
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof GameFragment.OnFragmentInteractionListener)
        {
            mListener = (GameFragment.OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mgr.unregisterListener(listener);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener
    {
        void onFragmentInteraction(Uri uri);
    }

    private SensorEventListener listener = new SensorEventListener()
    {
        @Override
        public void onSensorChanged(SensorEvent event)                                              // Wird aufgerufen sobald sich der Sensorwert ändert.
        {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)                                // wenn Accelerometer
            {
                float x, y;
                //int position_sign = 1;
                x = event.values[0];                                                                //schreibe die Sensorwerte in die float-variablen x,y,z
                y = event.values[1];


                if (Math.abs(y) > 1)
                {                                                               // neigungsgrenzwert 1 , dass sich das nicht immer bewegt
                    horizontalScrollView.setSmoothScrollingEnabled(true);
                    if (Math.abs(x) > 1)
                        position_sign = (int) (x / Math.abs(x) * -1);                                // Positionsabhängige vorzeichenänderung um eine gleichbleibende Scrollrichtung zu gewärleisten
                    horizontalScrollView.smoothScrollBy((int) Math.pow(y, 3) * position_sign, 0); // Funktion zum Scrollen in verschiedene richtungen/in verschiedenen geschwindigkeiten, unabhängig von der ausrichtung des SP

                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy)
        {

        }
    };


    /**
     * Karte ziehen
     */
    private void drawCard()
    {
        clearCards();
        gameControl.handleInput(0, 1, -1, -1);
        setCards();
        cpuPlay();
        setStatus(gameControl.getTurnOfPlayer());
    }

    /**
     * Löscht alle Karten aus dem Layout
     */
    private void clearCards()
    {
        LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.hand);
        linearLayout.removeAllViews();
    }

    /**
     * Setzt Handkarten neu
     */
    private void setCards()
    {
        if (host)
        {
            List<Card> cards;
            cards = gameControl.getCardsOfPlayer(playerNumber);
            setCards(cards);
        }
    }

    private void setCards(List<Card> cards)
    {
        LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.hand);
        UnoButton button;
        Card card;
        int identifier;
        Iterator<Card> it = cards.iterator();
        while (it.hasNext())
        {
            card = it.next();
            button = new UnoButton(getView().getContext(), card.id);
            button.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    playCard((UnoButton) v);
                }
            });
            identifier = getResources().getIdentifier("hand_" + card.id, "drawable", getView().getContext().getPackageName());
            button.setBackgroundDrawable(getResources().getDrawable(identifier));
            linearLayout.addView(button);
        }

    }

    private void playCard(UnoButton unoButton)
    {

        if (gameControl.handleInput(0, 0, unoButton.getCardID(), -1))
        {
            updateStack(unoButton.getCardID());
            removeCardFromHand(unoButton);      //Send to clients

            setStatus(gameControl.getTurnOfPlayer());
            checkGameOver();

            cpuPlay();
        }
        else        //invalid input
            return;


    }

    private void removeCardFromHand(UnoButton unoButton)
    {
        LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.hand);
        linearLayout.removeView(unoButton);
    }

    private void updateStack(int id)
    {
        Button stack = (Button) getView().findViewById(R.id.fragmentButtonStack);
        int identifier = getResources().getIdentifier("stapel_" + id, "drawable", getView().getContext().getPackageName());
        stack.setBackgroundDrawable(getResources().getDrawable(identifier));
    }


    private void finishGame(String message)
    {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
        getActivity().finish();
    }


    private void handleClientMessage(String clientMessage)
    {
        JSONObject clientJson = Json.StringToJSON(clientMessage);

        if (gameControl.handleResponse(clientJson))
        {
            updateStack(gameControl.getStackTopCard());
            setStatus(gameControl.getTurnOfPlayer());
            checkGameOver();
        }
    }

    private void cpuPlay()
    {
        while (gameControl.getTurnOfPlayer() != 0)
        {
            handleClientMessage(CpuPlayer.performAction(gameControl.getTurnOfPlayer(), gameControl).toString());
            checkGameOver();
        }
    }

    private void checkGameOver()
    {
        switch (gameControl.isGameOver())
        {
            case -1:
                break;             //Wenn Spiel nicht vorbei
            case 0:
                finishGame(getString(R.string.won));
                break; //Wenn Host gewonnen

            default:
                finishGame(getString(R.string.lost)); //Wenn Host verloren
        }
    }

}
