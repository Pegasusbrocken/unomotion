package de.maprog.unomotion.peer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import de.maprog.unomotion.activities.APeerActivity;
import de.maprog.unomotion.activities.ConnectActivity;
import de.maprog.unomotion.activities.HostActivity;

/**
 * Created by Konstantin on 24.11.2016.
 * Receiver für Wifi-Direct
 */
public class PeerReceiver extends BroadcastReceiver implements WifiP2pManager.ConnectionInfoListener
{
    WifiP2pManager mManager;
    WifiP2pManager.Channel mChannel;
    WifiP2pManager.PeerListListener listener;
    APeerActivity activity;

    public PeerReceiver(WifiP2pManager mManager, WifiP2pManager.Channel mChannel, WifiP2pManager.PeerListListener listener, APeerActivity activity)
    {
        this.mManager = mManager;
        this.mChannel =mChannel;
        this.listener = listener;
        this.activity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        String action = intent.getAction();
        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action))
        {
            // Determine if Wifi P2P mode is enabled or not, alert
            // the Activity.
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state != WifiP2pManager.WIFI_P2P_STATE_ENABLED)
            {
                Toast.makeText(activity.getApplication(), "Wifi not enabled", Toast.LENGTH_SHORT).show();
            }
        }
        else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action))
        {
            if (mManager != null)
            {
                mManager.requestPeers(mChannel, listener);          //Verfügbare Geräte
            }

        }
        else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action))
        {
            if(mManager == null)
                return;

            NetworkInfo networkInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

            if (networkInfo.isConnected())
            {
                mManager.requestConnectionInfo(mChannel, this);     //Es wurde verbunden, Infos holen
            }
            else
                Toast.makeText(activity.getApplication(), "Not Connected", Toast.LENGTH_SHORT).show();

        }
        else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action))
        {
            Toast.makeText(activity.getApplication(), "ThisDeviceChangedAction", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo info)
    {
        if(info.isGroupOwner)           //Wenn Host -> auf Clients hören
        {
            activity.startListening();
        }
        else                            //Ansonsten zum Host verbinden
        {
            activity.setHost(info.groupOwnerAddress.getHostAddress());
            ((ConnectActivity) activity).connectToSocket(info.groupOwnerAddress.getHostAddress());
        }

    }
}

