package de.maprog.unomotion.utility;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by Konstantin on 25.01.2017.
 */
public class CpuPlayer
{
    public static JSONObject performAction(int id, GameControl gameControl)
    {
        List<Card> cards = gameControl.getCardsOfPlayer(id);

        for(Card card: cards)
        {
            if(gameControl.cardIsValid(card.id))
                return Json.JSONToServer(id, 0, card.id, -1);
        }

        return Json.JSONToServer(id, 1, -1, -1);
    }


}
