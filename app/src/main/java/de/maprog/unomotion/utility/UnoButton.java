package de.maprog.unomotion.utility;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by Konstantin on 18.01.2017.
 * Abgeleiter Button, speichert zusätzlich noch die Id der Karte, die er abbildet.
 */

public class UnoButton extends Button
{
    int cardID;

    public UnoButton(Context context, AttributeSet attributeSet)
    {
        super(context,attributeSet);
    }

    public UnoButton(Context context, int cardID)
    {
        super(context);
        this.cardID = cardID;
    }

    public int getCardID()
    {
        return cardID;
    }

    public void setCardID(int cardID)
    {
        this.cardID = cardID;
    }
}
