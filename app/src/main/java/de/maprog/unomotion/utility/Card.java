package de.maprog.unomotion.utility;

/**
 * Created by Quiet on 20 Nov 16.
 */
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Stack;

public class Card {

    public int id=  -1;
    public int number = -1;
    public int cardType = -1; // 0 = Number, 1 = Reverse, 2 = Skip, 3 = WildCard, 4= +2, 5= +4 Wildcard
    public int color= -1;

    private Card(int CardType, int id)
    {
        this.id = id;
        this.cardType = CardType;
        if(CardType == 0)
        {
            this.number = (((id+(1+id/19))/2)%10);


            this.color = (id/19);
        }
        else if (CardType == 1||CardType == 2||CardType == 4)
        {
            this.color = id%4;
        }

    }

    public static Card[] generateDeck()
    {
       Card a ;
        Card[] Deck = new Card[108];
        int i = 0;
        for (i = 0; i <108 ; i++) {
            if(i<76)                        //NumberCards   76
            a = new Card(0,i);
            else if(i<84)                   //Reverse       8
                a = new Card(1,i);
            else if(i<92)                   //Skip          8
                a = new Card(2,i);
            else if(i<96)                   //WildCard      4
                a = new Card(3,i);
            else if(i<104)                  //+2            8
                a = new Card(4,i);
            else                             //+4           4
                a = new Card(5,i);
            Deck[i] = a;                    //Pack die Karte in das Deck an Stelle i
        }
        return Deck;
    }

    public static Card getCardByID(int id){
        Card a;
        if(id<76)                        //NumberCards   76
            a = new Card(0,id);
        else if(id<84)                   //Reverse       8
            a = new Card(1,id);
        else if(id<92)                   //Skip          8
            a = new Card(2,id);
        else if(id<96)                   //WildCard      4
            a = new Card(3,id);
        else if(id<104)                  //+2            8
            a = new Card(4,id);
        else if(id <108)                             //+4           4
            a = new Card(5,id);
        else a = null;
        return a;
    }

    public static Card[] shuffleCards(Card[] in_deck){
        Card tmp;
        int rand;
        Random r = new Random();
        for (int i = 0; i < in_deck.length; i++) {
            rand = r.nextInt(in_deck.length);
            tmp = in_deck[i];
            in_deck[i] = in_deck[rand];
            in_deck[rand] = tmp;
        }


        return in_deck;
    }

    public static Stack generateDeckStack()
    {
        Card[] Deck = shuffleCards(generateDeck());
        List<Card> list= Arrays.asList(Deck);
        Stack<Card> stack = new Stack<Card>();
        stack.addAll(list);
        return stack;
    }

    public String toString()
    {
        String color;
        String cardType;
        switch(this.color) {
            case 0:
                color = "Rot";
                break;
            case 1:
                color = "Gelb";
                break;
            case 2:
                color = "Grün";
                break;
            case 3:
                color = "Blau";
                break;
            default:
                color = "Farblos";
        }
        switch(this.cardType) {
            case 0:
                cardType = "Zahlenkarte";
                break;
            case 1:
                cardType = "Reverse-Karte";
                break;
            case 2:
                cardType = "Skip-Karte";
                break;
            case 3:
                cardType = "WildCard";
                break;
            case 4:
                cardType = "+2 Karte";
                break;
            case 5:
                cardType = "+4 Karte";
                break;
            default:
                cardType = "missingCardType";
        }

        return  "Number: " + this.number  + "Color: " + color  + "Type: " + cardType;
    }

}

/*
UNO DECK

19 numbers & 2x+2 &2x reverse &2x Skip per Color     (25 cards same color)
4 Wild Cards
4 x +4 Wild Cards
 */