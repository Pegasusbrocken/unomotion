package de.maprog.unomotion.utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Quiet on 05 Dec 16.
 * <p>
 * <p>
 * JSON
 * <p>
 * JSON to Client:
 * - ToClientX(ID)
 * - ResponseType ( failure, success, win! ,lose)   ( hast karte gelegt die )
 * - its Players X Turn
 * - Amount of Players
 * - Players Amount of Cards
 * - Your Cards(all of em)
 * - Cards on Stack
 * - YouHaveToDrawXCards-Stack // braucht er den?
 * - ColorWish // bei Wildcard gewünschte Farbe
 * <p>
 * JSON to SERVER
 * <p>
 * - clientNr // NUR wenn diese nicht von WLAN modul bestimmt werden kann.(lass das lieber nicht die Clienten entscheiden, welcher client sie sind...)
 * - TurnType (Draw card, Lay down a card)
 * - Card X(ID) Placed
 * - ColorWish // bei Wildcard gewünschte Farbe
 * - Leave?
 */

public class Json
{

    public static JSONObject StringToJSON(String json)
    {
        try
        {
            JSONObject reader = new JSONObject(json);
            return reader;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;

    }

    public static JSONObject JSONToServer(int client, int turnType, int cardID, int ColorWish)
    {
        /*
        *  Turn Type
        *  0 = Karte Legen, => Card ID != -1, je nach Card ID auch ColorWish != -1
        *  1 = Karte Ziehen => CardID == Colorwish == -1
        * */
        try
        {
            JSONObject JSON_Response = new JSONObject();
            JSON_Response.put("client", client);
            JSON_Response.put("response", turnType);
            JSON_Response.put("card", cardID);
            JSON_Response.put("colorWish", ColorWish);
            return JSON_Response;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static JSONObject JSONToClient(int client, int responseType, int turnOfPlayer, int[] cardsOfClient, int cardOnStack, int colorWish)
    {
        try
        {
            JSONObject JSON_Response = new JSONObject();
            JSON_Response.put("client", client);
            JSON_Response.put("responseType", responseType);
            JSON_Response.put("turn", turnOfPlayer);
            JSON_Response.put("cardOnStack", cardOnStack);

            JSONArray JSONCardsOfClient = new JSONArray();
            for (int i = 0; i < cardsOfClient.length; i++)
            {
                JSONCardsOfClient.put(cardsOfClient[i]);
            }
            JSON_Response.put("cards", JSONCardsOfClient);
            JSON_Response.put("ColorWish", colorWish);


            return JSON_Response;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;

        //JSON_Response.put("toDraw",toDrawStack);                                                  // Wenn der ToDrawStack angezeigt werden soll
        // Players Cards array,2D, array.length = amount of players
        // YourCards
        // CardsOnStack
        /*
        JSONArray JSONCardsPerPlayer = new JSONArray();
        for (int i = 0; i < CardsPerPlayerArray.length; i++) {
            JSONCardsPerPlayer.put(CardsPerPlayerArray[i]);
        }
        */
        // JSON_Response.put("cardsPerPlayer",JSONCardsPerPlayer);                                   // Für die Anzeige Wieviele Karten die jeweiligen Teilnehmer besitzen.
    }

}
