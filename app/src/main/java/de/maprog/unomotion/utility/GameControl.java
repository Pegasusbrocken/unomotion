package de.maprog.unomotion.utility;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by Quiet on 06 Dec 16.
 */

public class GameControl
{
    private final int AMOUNT_OF_STARTING_CARDS = 7;
    private int players;
    // protected int[][] cardsOfPlayers;
    private List<Card> cardsOfPlayer1;
    private List<Card> cardsOfPlayer2;
    private List<Card> cardsOfPlayer3;
    private List<Card> cardsOfPlayer4;
    private Stack<Card> deck = new Stack<Card>();
    private Stack<Card> stack = new Stack<Card>();
    private int turnOfPlayer;
    private int toDrawStack;
    private int colorWish;                                                // -1: nothing, O: Rot 1:Gelb 2:Grün 3: Blau
    private int direction = 1;


    public GameControl(int numberOfPlayers)
    {
        this.players = numberOfPlayers;
        //     this.cardsOfPlayers = new int[numberOfPlayers][128];                                        // Hey, 's kann sein, dass das erstmal vorinitialisiert werden muss, mit -1 oder so ( ja -1 klingt gut)
        initHands();
        this.deck = Card.generateDeckStack(); // Todo Call by What?
        this.turnOfPlayer = (int) (Math.random() * numberOfPlayers);     // Generates player 0 to 3
        this.toDrawStack = 0;
        this.colorWish = -1;


        for (int i = 0; i < AMOUNT_OF_STARTING_CARDS; i++)
        {                                           // teilt jedem Spieler <AMOUNT_OF_STARTING_CARDS> Karten aus
            cardsOfPlayer1.add(deck.pop());
            if (this.players > 1)
                cardsOfPlayer2.add(deck.pop());
            if (this.players > 2)
                cardsOfPlayer3.add(deck.pop());
            if (this.players > 3)
                cardsOfPlayer4.add(deck.pop());

        }
        stack.push(deck.pop());
    }

    /**
     * Muss ja erstmal initialisiert werden.
     * Es bleibt sich gleich, ob man nur die benötigten Initialisiert, oder gleich alle.
     * Im schlimmsten Fall werden sie eben nicht weiter benutzt.
     */
    private void initHands()
    {
        cardsOfPlayer1 = new ArrayList<>();
        cardsOfPlayer2 = new ArrayList<>();
        cardsOfPlayer3 = new ArrayList<>();
        cardsOfPlayer4 = new ArrayList<>();
    }


    //Wenn es möglich ist, dass wir ermitteln können (wlan - name/ID) von wem die Nachricht war, dann:
    //public static int handleResponse(JSONObject ClientResponse, int clientID)
    public boolean handleResponse(JSONObject clientResponse)
    {
        boolean result = false;
        int clientID = -10;
        int responseType = -10;                                                                           // 0: Spieler hat Karte gelegt.  1: Spieler hat Karte Gezogen  -1 Spieler hat Spiel Verlassen.(todo implement -1)
        int cardID = -10;
        int colorWish = -10;
        try
        {
            clientID = clientResponse.getInt("client");
            responseType = clientResponse.getInt("response");
            cardID = clientResponse.getInt("card");
            colorWish = clientResponse.getInt("colorWish");
        }
        catch (Exception e)
        {
            e.printStackTrace(); // Macht das Sinn?
            //TODO Erstelle einen FehlerBroadcast. Bei Fehler, alle senden ihren letzten response erneut. (meinetwegen auch nur der letzte client bzw der in der nachricht spezifizierte)
        }

        if (handleInput(clientID, responseType, cardID, colorWish))
            result = true;

        return result;
    }


    public boolean handleInput(int clientID, int responseType, int cardId, int colorWish)
    {
        boolean result = false;

        if (clientID == turnOfPlayer)
        {
            switch (responseType)
            {
                case 0:                                                                             //Karte Gelegt.
                    if (cardIsValid(cardId))
                        if (placeCard(clientID, cardId) == 0)   //wenn karte nicht gelegt werden konnte(weil sie nicht in den Handkarten vorhanden war)
                        {
                            result = true;
                            nextTurn();
                        }
                    break;

                case 1:
                    drawCards(clientID);
                    nextTurn();
                    result = true;
                    break;
            }

        }

        return result;
    }


    public List<Card> drawCards(int clientID)
    {                                                           //ziehe Karten, wenn toDrawStack =0, Zeihe eine, Wenn toDrawStack>0 ziehe toDrawStack
        List<Card> drawnCards = new ArrayList<>();
        if (deck.size() < toDrawStack || deck.size() < 1)
            stackToDeck();
        int drawAmount = 1;
        if (toDrawStack > 0)
        {
            drawAmount = toDrawStack;
            toDrawStack = 0;
        }

        for (int i = 0; i < drawAmount; i++)
        {
            drawnCards.add(drawCard(clientID));
        }
        return drawnCards;
    }

    public Card drawCard(int clientID)
    {
        if (clientID == 0)
            cardsOfPlayer1.add(deck.peek());
        if (clientID == 1)
            cardsOfPlayer2.add(deck.peek());
        if (clientID == 2)
            cardsOfPlayer3.add(deck.peek());
        if (clientID == 3)
            cardsOfPlayer4.add(deck.peek());
        return deck.pop();

    }

    private void stackToDeck()
    {                                                                       //Takes the stack and turns it into the deck(todo)
        Stack<Card> temp_deck = new Stack<Card>();
        Stack<Card> temp_stack = new Stack<Card>();
        while (!deck.isEmpty())                                                                          // nehme restliche Karten vom deck
            temp_deck.push(deck.pop());
        temp_stack.push(stack.pop());                                                                   //speichere oberste karte vom stack
        while (!stack.isEmpty())
            deck.push(stack.pop());                                                                 // lege stack in das deck
        shuffleDeck();                                                                              //mische deck
        while (!temp_deck.isEmpty())
            deck.push(temp_deck.pop());                                                             // lege altes Deck auf das gemischte drauf
        stack.push(temp_stack.pop());                                                                //gespeicherte Stack karte ist nun der Komplette stack.

    }

    private void shuffleDeck()
    {
        //TODO
        // man neme das Deck (this.deck) und mischt es ganz gut durch.
    }

    public boolean cardIsValid(int cardID)
    {                                                       // i know, that one i pretty tricky, but try to valdidate that funktion. (todo)
        Card PlayersCard = Card.getCardByID(cardID);                                                        //Todo Das funktioniert so nicht, schreibe lookup funktion die aus einem referenzdeck die Karten nummt (mit generateDeck)
        Card StackCard = stack.peek();

        if (PlayersCard.cardType == 5)                                                                 // +4 karten können immer gelegt werden
            return true;
        if (this.toDrawStack > 0 && PlayersCard.cardType == 4 && (PlayersCard.cardType == StackCard.cardType || (PlayersCard.color == StackCard.color) || (PlayersCard.color == colorWish) || StackCard.color == -1/* weil kein colorwish*/))                         // +2 karten können nur gelegt werden wenn sie die richtige farbe haben
            return true;
        if (this.toDrawStack > 0)                   // Wenn karten zu ziehen sind, können nur +x Karten gelegt werden.
            return false;
        if ((StackCard.cardType == 3 || StackCard.cardType == 5) /*&& PlayersCard.color == colorWish*/)      // Wenn letzte karte Wildcard, dann muss Farbe = Colorwish sein, dann Valid. Auskommentiert, da keine Farbauswahl nach WildCard
            return true;
        if ((PlayersCard.cardType == 0 || PlayersCard.cardType == 1 || PlayersCard.cardType == 2 || PlayersCard.cardType == 4) && (StackCard.color == PlayersCard.color)) // Wenn Number,Reverse,Skip oder +2 karte, dann is valid, falls Stack.Color==Player.Color
            return true;
        if (PlayersCard.cardType == 0 && PlayersCard.number == StackCard.number || ((PlayersCard.cardType == 1 || PlayersCard.cardType == 2) && (PlayersCard.cardType == StackCard.cardType)))                                                  //wenn gleiche Nummern. oder gleiche symbole
            return true;
        if (PlayersCard.cardType == 3)                                  //wenn wildcard.
            return true;
        return false;
    }

    public int placeCard(int clientID, int cardID)
    {                                              // entfernt die Karte aus der Hand des Spielers und legt sie auf den Stack, wenn die Karte eine +2/+4 Karte war, so erhöhe den To draw stack;
        if (clientID == 0)
        {
            for (int i = 0; i < cardsOfPlayer1.size(); i++)
            {                                          //Iteriere durch die Handkarten des Spielers
                if (cardsOfPlayer1.get(i).id == cardID)
                {                                                // Wenn karte in den Handkarten vorhanden
                    stack.push(cardsOfPlayer1.remove(i));                                                //entferne sie und lege sie auf den Stack
                    if (stack.peek().cardType == 1)                                                  //Reverse
                        swapDirection();
                    if (stack.peek().cardType == 2)                                                  //Stop
                        nextTurn();
                    if (stack.peek().cardType == 3)                                                  //Wildcard
                        this.colorWish = -1/*(int) (Math.random() * 4)*/;                                         // maybe noch ändern   0,1,2,3 => ROT,GELB,GRÜN,BLAU
                    else
                        this.colorWish = -1;                                                         // wenn keine WildCard, dann colorwish = -1
                    if (stack.peek().cardType == 4)                                                  //+2
                        toDrawStack += 2;
                    if (stack.peek().cardType == 5)                                                  //+4
                        toDrawStack += 4;
                    return 0;
                }
            }
        }

        if (clientID == 1)
        {
            for (int i = 0; i < cardsOfPlayer2.size(); i++)
            {                                          //Iteriere durch die Handkarten des Spielers
                if (cardsOfPlayer2.get(i).id == cardID)
                {                                                // Wenn karte in den Handkarten vorhanden
                    stack.push(cardsOfPlayer2.remove(i));                                                //entferne sie und lege sie auf den Stack                    if (stack.peek().cardType == 1)                                                  //Reverse
                    swapDirection();
                    if (stack.peek().cardType == 2)                                                  //Stop
                        nextTurn();
                    if (stack.peek().cardType == 3)                                                  //Wildcard
                        colorWish = -1/*(int) (Math.random() * 4)*/;                                         // maybe noch ändern   0,1,2,3 => ROT,GELB,GRÜN,BLAU
                    else
                        colorWish = -1;                                                             // wenn keine WildCard, dann colorwish = -1
                    if (stack.peek().cardType == 4)                                                  //+2
                        toDrawStack += 2;
                    if (stack.peek().cardType == 5)                                                  //+4
                        toDrawStack += 4;
                    return 0;
                }
            }
        }
        if (clientID == 2)
        {
            for (int i = 0; i < cardsOfPlayer3.size(); i++)
            {                                          //Iteriere durch die Handkarten des Spielers
                if (cardsOfPlayer3.get(i).id == cardID)
                {                                                // Wenn karte in den Handkarten vorhanden
                    stack.push(cardsOfPlayer3.remove(i));                                                //entferne sie und lege sie auf den Stack
                    if (stack.peek().cardType == 1)                                                  //Reverse
                        swapDirection();
                    if (stack.peek().cardType == 2)                                                  //Stop
                        nextTurn();
                    if (stack.peek().cardType == 3)                                                  //Wildcard
                        colorWish = -1/*(int) (Math.random() * 4)*/;                                         // maybe noch ändern   0,1,2,3 => ROT,GELB,GRÜN,BLAU
                    else
                        colorWish = -1;                                                             // wenn keine WildCard, dann colorwish = -1
                    if (stack.peek().cardType == 4)                                                  //+2
                        toDrawStack += 2;
                    if (stack.peek().cardType == 5)                                                  //+4
                        toDrawStack += 4;
                    return 0;
                }
            }
        }
        if (clientID == 3)
        {
            for (int i = 0; i < cardsOfPlayer4.size(); i++)
            {                                          //Iteriere durch die Handkarten des Spielers
                if (cardsOfPlayer4.get(i).id == cardID)
                {                                                // Wenn karte in den Handkarten vorhanden
                    stack.push(cardsOfPlayer4.remove(i));                                                //entferne sie und lege sie auf den Stack
                    if (stack.peek().cardType == 1)                                                  //Reverse
                        swapDirection();
                    if (stack.peek().cardType == 2)                                                  //Stop
                        nextTurn();
                    if (stack.peek().cardType == 3)                                                  //Wildcard
                        colorWish = -1/*(int) (Math.random() * 4)*/;                                        // maybe noch ändern   0,1,2,3 => ROT,GELB,GRÜN,BLAU
                    else
                        colorWish = -1;                                                             // wenn keine WildCard, dann colorwish = -1
                    if (stack.peek().cardType == 4)                                                  //+2
                        toDrawStack += 2;
                    if (stack.peek().cardType == 5)                                                  //+4
                        toDrawStack += 4;
                    return 0;
                }
            }
        }
        return -1;
    }

    public int isGameOver()
    {
        if (cardsOfPlayer1.size() == 0 && players >= 1)
            return 0;
        if (cardsOfPlayer2.size() == 0 && players >= 2)
            return 1;
        if (cardsOfPlayer3.size() == 0 && players >= 3)
            return 2;
        if (cardsOfPlayer4.size() == 0 && players >= 4)
            return 3;
        return -1;
    }

    public void nextTurn()
    {                                                                                // nächster Spieler ist am Zug
        this.turnOfPlayer += direction;

        if (this.turnOfPlayer == (players))
            this.turnOfPlayer = 0;
        if (this.turnOfPlayer == -1)
            this.turnOfPlayer = players - 1;
    }

    private void swapDirection()
    {
        this.direction *= -1;
    }

    public List<Card> getCardsOfPlayer(int clientID)
    {
        if (clientID == 0)
        {
            return this.cardsOfPlayer1;
        }
        if (clientID == 1)
        {
            return this.cardsOfPlayer2;
        }
        if (clientID == 2)
        {
            return this.cardsOfPlayer3;
        }
        if (clientID == 3)
        {
            return this.cardsOfPlayer4;
        }

        return null;
    }

    public int[] getCardsOfPlayernAsIDs(int clientID)
    {

        if (clientID == 0)
        {
            int[] HandkartenIDs = new int[this.cardsOfPlayer1.size()];
            for (int i = 0; i < this.cardsOfPlayer1.size(); i++)
            {
                HandkartenIDs[i] = this.cardsOfPlayer1.get(i).id;
            }
            return HandkartenIDs;
        }
        if (clientID == 1)
        {
            int[] HandkartenIDs = new int[this.cardsOfPlayer2.size()];
            for (int i = 0; i < this.cardsOfPlayer2.size(); i++)
            {
                HandkartenIDs[i] = this.cardsOfPlayer2.get(i).id;
            }
            return HandkartenIDs;
        }
        if (clientID == 2)
        {
            int[] HandkartenIDs = new int[this.cardsOfPlayer3.size()];
            for (int i = 0; i < this.cardsOfPlayer3.size(); i++)
            {
                HandkartenIDs[i] = this.cardsOfPlayer3.get(i).id;
            }
            return HandkartenIDs;
        }
        if (clientID == 3)
        {
            int[] HandkartenIDs = new int[this.cardsOfPlayer4.size()];
            for (int i = 0; i < this.cardsOfPlayer4.size(); i++)
            {
                HandkartenIDs[i] = this.cardsOfPlayer4.get(i).id;
            }
            return HandkartenIDs;
        }

        return null;
    }


    public int getStackTopCard()
    {
        return this.stack.peek().id;
    }

    public int getToDrawStack()
    {
        return this.toDrawStack;
    }

    public int getColorWish()
    {
        return this.colorWish;
    }

    public int getTurnOfPlayer()
    {
        return this.turnOfPlayer;
    }

    public int[] cardsPerPlayer()
    {
        int[] cardsArray = new int[4];
        cardsArray[0] = this.cardsOfPlayer1.size();
        cardsArray[1] = this.cardsOfPlayer2.size();
        cardsArray[2] = this.cardsOfPlayer3.size();
        cardsArray[3] = this.cardsOfPlayer4.size();

        return cardsArray;
    }

}
